var config = require('config');
var express = require("express");
var app = express();
var connection = require('./routes/mySqlLib');
var index=require('./routes/index');
var users=require('./routes/users');

var bodyParser = require('body-parser');
app.use(bodyParser.json());       
app.use(bodyParser.urlencoded({     
  extended: true
})); 

app.set('view engine', 'jade');

app.get('/',index);


app.post('/forgot' , users);

app.post('/reset' , users);

app.listen(config.get('PORT'));
console.log("Express server started on port %d", config.get('PORT'));