/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var constant = require('./constant');

exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse, res);
};
exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse, res);
};
exports.emailNotRegistered = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMAIL_NOT_REGISTERED,
        message: constant.responseMessage.EMAIL_NOT_REGISTERED,
        data: {}
    }
    sendData(errResponse, res);
};


exports.sendError = function (res) {

    var errResponse = {
        status: constant.responseStatus.SHOW_ERROR_MESSAGE,
        message: constant.responseMessage.SHOW_ERROR_MESSAGE,
        data: {}
    }
    sendData(errResponse, res);
};

exports.linkExpire = function (res) {

    var errResponse = {
        status: constant.responseStatus.SHOW_LINK_ERROR_MESSAGE,
        message: constant.responseMessage.SHOW_LINK_ERROR_MESSAGE,
        data: {}
    }
    sendData(errResponse, res);
};

exports.sendSuccessData = function (res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESSFUL_EXECUTION,
        message: constant.responseMessage.SUCCESSFUL_EXECUTION,
        data: {}
    }
    sendData(successResponse, res);
};
exports.mailSent = function (res) {

    var successResponse = {
        status: constant.responseStatus.MAIL_SENT,
        message: constant.responseMessage.MAIL_SENT,
        data: {}
    }
    sendData(successResponse, res);
};


exports.sendData = function (data, res) {
    sendData(data, res);
};


function sendData(data, res) {
    res.type('json');
    res.jsonp(data);
}