var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var express = require('express');
var router = express.Router();
var generatePassword = require('password-generator');
var moment = require('moment');
var md5 = require('MD5');
var request = require("request");
var http = require("http");
var async = require("async");


router.post('/forgot', function (req, res) {
    var email = req.body.email;

    var manValues = [email];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        },
        function (callback) {
            func.checkEmailAvailability(res, email, callback);

        }], function (updatePopup) {

        var sql = "SELECT user_id,Fname FROM tb_users WHERE email=? LIMIT 1";
        connection.query(sql, [email], function (err, response12) {

            if (response12.length === 1) {
                func.sendMailToCustomer(email, res);

            }
            else {

                sendResponse.sendError(res);
            }

        });
    });
});
router.post('/reset', function (req, res) {
    var link = req.body.link;
    var new_password = req.body.pass;
    var manValues = [link, new_password];

    var checkData = func.checkBlank(manValues);
    if (checkData === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
        connection.query(sql, [link], function (err, result5) {

            if (!err) {
                var md5 = require('MD5');
                var hash = md5(new_password);
                var sql = "SELECT `password_reset` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result57) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (result57[0].password_reset == 0) {


                            var sql2 = "UPDATE `tb_users` SET `one_time_link`=?,`encryptPassword`=?,`password_reset`=? WHERE `user_id`=? LIMIT 1"

                            connection.query(sql2, [link, hash, 1, result5[0].user_id], function (err, result56) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {

                                    sendResponse.sendSuccessData(res);
                                }

                            });
                        }
                        else {
                            sendResponse.linkExpire(res);
                        }
                    }

                });
            }
            else {
                sendResponse.somethingWentWrongError(res);
            }
        });


    }

});


module.exports = router;