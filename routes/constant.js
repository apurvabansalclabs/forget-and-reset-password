/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 101);
define(exports.responseStatus, "EMAIL_NOT_REGISTERED", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SUCCESSFUL_EXECUTION", 104);
define(exports.responseStatus, "MAIL_SENT", 105);
define(exports.responseStatus, "SHOW_LINK_ERROR_MESSAGE", 106);


exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Password Changed Successfully.");
define(exports.responseMessage, "MAIL_SENT", "Mail Sent Successfully.");
define(exports.responseMessage, "SHOW_LINK_ERROR_MESSAGE", "Sorry the link has expired");





