var sendResponse = require('./sendResponse');
var config = require('config');
var generatePassword = require('password-generator');

exports.checkBlank = function (arr) {

    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === '' || arr[i] === undefined || arr[i] === '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
};
exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {

            callback(null);
        }
        else {
            sendResponse.emailNotRegistered(res);

        }
    });
}

exports.sendMailToCustomer = function (email, res) {
    var password = generatePassword(6, false);
    var md5 = require('MD5');
    var encrypted_pass = md5(password);
    var to = email;
    var sub = "Password Reset";
    var html = 'Information to reset your password. Copy the link below to reset your password. This link is valid for one use only.' + encrypted_pass;

    sendEmail(to, html, sub, function (result) {

        if (result == 1) {

            var sql3 = "UPDATE `tb_users` SET `one_time_link`=?,`password_reset`=? WHERE `email`=? LIMIT 1"

            connection.query(sql3, [encrypted_pass, 0, to], function (err, result4) {

                if (err) {
                    sendResponse.sendError(res);
                }
                else {
                    sendResponse.mailSent(res);
                }

            });

        }


        else {

            sendResponse.sendError(res);
        }
    });

}
function sendEmail(receiverMailId, message, subject, callback) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password

        }

    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message

    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, info) {

        if (error) {
            return callback(0);

            console.log(error);
        } else {

            return callback(1);
            console.log('Message sent');
        }
    });
}
;

